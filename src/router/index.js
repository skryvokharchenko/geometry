import Vue from 'vue'
import VueRouter from 'vue-router'
//import Home from '../views/Home.vue'
import s12 from '../views/s1-2.vue'
import s13 from '../views/s1-3.vue'
import s14 from '../views/s1-4.vue'
import s15 from '../views/s1-5.vue'
import s24 from '../views/s2-4.vue'
import s312 from '../views/s3-12.vue'
import s313 from '../views/s3-13.vue'
import s314 from '../views/s3-14.vue'
import s315 from '../views/s3-15.vue'
import s316 from '../views/s3-16.vue'
import s317 from '../views/s3-17.vue'
import s318 from '../views/s3-18.vue'
import s319 from '../views/s3-19.vue'
import s320 from '../views/s3-20.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/s1-2',
    name: 's1-2',
    component: s12
  },
  {
    path: '/s1-3',
    name: 's1-3',
    component: s13
  },
  {
    path: '/s1-4',
    name: 's1-4',
    component: s14
  },
  {
    path: '/s1-5',
    name: 's1-5',
    component: s15
  },
  {
    path: '/s2-4',
    name: 's2-4',
    component: s24
  },
  {
    path: '/',
    name: 's3-12',
    component: s312
  },
  {
    path: '/s3-13',
    name: 's3-13',
    component: s313
  },
  {
    path: '/s3-14',
    name: 's3-14',
    component: s314
  },
  {
    path: '/s3-15',
    name: 's3-15',
    component: s315
  },
  {
    path: '/s3-16',
    name: 's3-16',
    component: s316
  },
  {
    path: '/s3-17',
    name: 's3-17',
    component: s317
  },
  {
    path: '/s3-18',
    name: 's3-18',
    component: s318
  },
  {
    path: '/s3-19',
    name: 's3-19',
    component: s319
  },
  {
    path: '/s3-20',
    name: 's3-20',
    component: s320
  },
]

const router = new VueRouter({
  routes,
  mode: 'history'
})

export default router
